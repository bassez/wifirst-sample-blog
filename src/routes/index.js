import React from 'react';
import { Switch, Route } from 'react-router-dom'

import Posts from '../pages/posts/posts';
import Users from '../pages/users/users';
import UserDetail from '../pages/userDetail/userDetail';
import PostDetail from '../pages/postDetail/postDetail';

export default function Routes() {
	return(
		<Switch>

			<Route path="/" exact component={Posts} />
			<Route path="/users"  component={Users} />
			<Route path="/user/:userId" component={UserDetail} />
			<Route path="/post/:postId" component={PostDetail} />
			<Route component={Posts} />
		</Switch>
	)
}