import React, { Component }  from 'react';
import { Link } from 'react-router-dom'

import Menu from '../../menu/Menu'
import Loader from '../../loader/loader'
import Error from '../error/error'

import './posts.css';


class Posts extends Component {

  	constructor(props) {
    	super(props)
	    this.state = { 
	    	posts: null,
	    	users: null,
        error: null,
	    	isPostsLoading: false, 
	    	isUsersLoading: false, 
	    }
    }

    componentDidMount() {
		  this.setState({ isPostsLoading: true, isUsersLoading: true });

    	fetch('https://jsonplaceholder.typicode.com/posts')
      		.then(response => response.json())
      		.then(posts => this.setState({ posts, isPostsLoading:false}))
          .catch(error => this.setState({ error, isPostsLoading: false }));

    	fetch('https://jsonplaceholder.typicode.com/users')
      		.then(response => response.json())
      		.then(users => this.setState({ users, isUsersLoading:false }))
          .catch(error => this.setState({ error, isUsersLoading: false }));
    }

  	getUserInfoFromUserId(userId, isOnlyUserName = false){
  		const { users } = this.state;
  		if(users){

	  		const user = users.find(user => user.id === userId);
        
        if(isOnlyUserName){
    			return user.name;
        }

        return user;
  		}
  	}

  	render(){
  		const { posts, isPostsLoading, isUsersLoading, error } = this.state;
  		const isDataLoading = isPostsLoading && isUsersLoading;
      let postsList;

      if (error) {
        return <Error error={error.message} />;
      }

  		if(posts){

	  		postsList = <div className="postList">
	  			{ posts.map(({ id, userId, title, body }) => (
           
            <Link key={id} className="postLink" to={{
                pathname:`/post/${id}`, 
                post: { id, userId, title, body, user: this.getUserInfoFromUserId(userId)}
              }}>
	 		        
              <div className="postWrappBox"> 
                <Link className="postLink" to={{pathname: `/user/${userId}`, user: this.getUserInfoFromUserId(userId)}}>
                  Author : {this.getUserInfoFromUserId(userId, true)}
                </Link>
  		        	<p>Title : {title}</p>
  			        <p>Comment : {body}</p>
              </div>
		        
            </Link>

				))}
		    </div>;
  		}

  		return <div className="postsWrapper">
  			<Menu/>
   			<h1>Post list forum</h1>
        <h2>Click on post for post detail</h2>
        <h2>Click on author for user detail</h2>
   			{
   				isDataLoading 
   					? <Loader/>
   					: postsList
   			}
  		</div>
  	};
}

export default Posts;
