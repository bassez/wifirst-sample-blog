import React, { Component }  from 'react';

import Menu from '../../menu/Menu'

import './userDetail.css';


class UserDetail extends Component {

  	constructor(props) {
      super(props)
      this.state = {
        user : null, 
        errorLoading: false,
      }
    }

    componentDidMount() {
      if(this.props.location.user){
        this.setState({ user: this.props.location.user});
      }
      // else { 
        //Meme soucis que sur postDetails.js
        //Si on rafraichit la page on perd le user dans les props ainsi que le post ce qui provoque une erreur.
        //Il faudrait idéalement un 5eme endpoint '/user/:id' car je ne sais pas comment récupérer le nom de l'auteur du post
        //sans re-parcourir tout les users avec un fetch et récupérer le nom de l'auteur via un copier coller 
        //de la fonction getUserInfoFromUserId du fichier posts.js ce qui je pense ferais une duplication de code 
        //et une requete supplémentaire sur tout les users juste pour un user ?;
      // }

    }

    getUserFullAddress(address){
      const {street, suite, city, zipcode} = address;
      return `${suite} ${street} ${city} ${zipcode}`;
    }

  	render(){
      const {user} = this.props.location;

  		return <div>
  			<Menu/>
   			<h1>UserDetail</h1>
        <div className="userWrapper">
          {user 
            ? <div className="userInfo">
            <p>Name : {user.name}</p>
            <p>Username : {user.username}</p>
            <p>Email : {user.email}</p>
            <p>Adress : {this.getUserFullAddress(user.address)}</p>
            <p>Phone : {user.phone}</p>
            <p>Website : {user.website}</p>
            <p>CompanyName : {user.company.name}</p>
            <p>CatchPhrase : {user.company.catchPhrase}</p>
            <p>Bs : {user.company.bs} </p>
          </div> 
            : <div> No User Data </div>

          }
        </div>
        
        
  		</div>
  	};
}

export default UserDetail;