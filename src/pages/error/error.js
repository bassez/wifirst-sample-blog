import React from 'react';

import './error.css';

function error(props){
	
	const {error} = props;
	return <div className="errorWrapper">
		<div className="error">
			{ error && error.message ? error.message : 'An error happened' }
		</div>
	</div>
}

export default error;