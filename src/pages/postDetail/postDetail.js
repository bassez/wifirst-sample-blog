import React, { Component }  from 'react';
import { Link }  from 'react-router-dom';

import Menu from '../../menu/Menu'
import Loader from '../../loader/loader'
import Error from '../error/error'

import './postDetail.css';


class PostDetail extends Component {

  	constructor(props) {
    	super(props)
	    this.state = {
        comments : null, 
        post: null,
        commentsLoading: false,
        error: false,
      }
    }

    componentDidMount() {
      let postId;
      if(this.props.location.post){
        const { post } = this.props.location
        this.setState({ post });
        postId = post.id;
      }
      // else { 
        //Si on rafraichit la page on perd l'auteur du post dans les props ainsi que le post.
        //Il faudrait idéalement un 5eme endpoint '/user/:id' car je ne sais pas comment récupérer le nom de l'auteur du post
        //sans re-parcourir tout les users avec un fetch et récupérer le nom de l'auteur via un copier coller 
        //de la fonction getUserInfoFromUserId du fichier posts.js ce qui je pense ferais une duplication de code 
        //et une requete supplémentaire sur tout les users juste pour un user.name?;
      // }
        
      this.setState({ commentsLoading: true});
  
      fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
        .then(response => response.json())
        .then(comments => this.setState({ comments, commentsLoading:false }))
        .catch(error => this.setState({ error, commentsLoading: false }));
    
    }

  	render(){
      const { commentsLoading, comments, error, post } = this.state;
      let commentsList, postDetail;

      if (error) {
        return <Error error={error.message} />;
      }
      
      if(post){
        const { userId, title, body, user } = post;
        
        postDetail = <div className="postDetail">
            <Link className="postLink" to={{pathname: `/user/${userId}`, user}}>
              Author : {user.name}
            </Link>
            <p>Title : {title}</p>
            <p>Content : {body} </p>
          </div>
      }

      if(comments){

        commentsList = <div className="commentList">
          { comments.map(({ id, name, email, body }) => (            
              <div key={id} className="commentWrappBox"> 
                <p>name : {name}</p>
                <p>email : {email}</p>
                <p>Content : {body}</p>              
              </div>

        ))}
        </div>;
      }
    
  		return <div className="postsWrapper">
  			<Menu/>
   			<h1>PostDetail</h1>
        <h2>Click on author for user detail</h2>
        <div className="postInfoWrapper">
          {postDetail}
        </div>
        <h2>Comments list</h2>
        {
          commentsLoading 
            ? <Loader/>
            : commentsList
        }
  		</div>
  	};
}

export default PostDetail;
