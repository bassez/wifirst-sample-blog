import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import Menu from '../../menu/Menu'
import Error from '../error/error';
import Loader from '../../loader/loader';
import './users.css';


class Users extends Component {

	constructor(props) {
    	super(props)
	    this.state = { 
	    	users: null,
	        error: null,
	    	isUsersLoading: false, 
	    }
    }

	componentDidMount() {
		this.setState({ isUsersLoading: true });

    	fetch('https://jsonplaceholder.typicode.com/users')
      		.then(response => response.json())
      		.then(users => this.setState({ users, isUsersLoading:false }))
          	.catch(error => this.setState({ error, isUsersLoading: false }));
    }

    getUserFullAddress(address){
      const {street, suite, city, zipcode} = address;
      return `${suite} ${street} ${city} ${zipcode}`;
    }

 	render(){
 		const {users, isUsersLoading, error} = this.state;
 		let usersList;
  		if (error) {
        	return <Error error={error.message} />;
      	}

      	if(users){

	  		usersList = <div className="usersList">
	  			{ users.map(({ id, name, username, email, address, phone, website, company }) => (

	          		<div key={id} className="userInfo">
			            <Link className="userLink" to={{pathname: `/user/${id}`, user: { name, username, email, address, phone, website, company }}}>
		                  Name : {name}
		                </Link>
			            <p>Username : {username}</p>
			            <p>Email : {email}</p>
			            <p>Adress : {this.getUserFullAddress(address)}</p>
			            <p>Phone : {phone}</p>
			            <p>Website : {website}</p>
			            <p>CompanyName : {company.name}</p>
			            <p>CatchPhrase : {company.catchPhrase}</p>
			            <p>Bs : {company.bs} </p>
		            </div>
          		))}
        	</div>
  		}

  		return <div className="usersWrapper">
  			<Menu/>
   			<h1>Users</h1>
   			<h2>Click on User Name for user detail</h2>
   			{
   				isUsersLoading 
   					? <Loader/>
   					: usersList
   			}
  		</div>
	}
}

export default Users;
