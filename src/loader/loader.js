import React from 'react';

import './loader.css'

function Loader () {
	return <div className="loader">
		<div className="loaderContent">Loading...</div>
	</div>
}

export default Loader;
 