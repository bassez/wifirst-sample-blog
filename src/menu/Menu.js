import React from 'react';
import { Link } from 'react-router-dom'
import './Menu.css';

function Menu() {
 
    return <div className="menuWrapper">
        <Link className="menuButton" to="/posts"> Posts </Link>
        <Link className="menuButton" to="/users"> Users </Link>
    </div>
}

export default Menu;